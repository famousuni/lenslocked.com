package controllers

import (
	"net/http"
	"net/url"

	"github.com/gorilla/schema"
)

func parseForm(r *http.Request, dst interface{}) error {
	//Parseform populates r.Form and r.PostForm
	if err := r.ParseForm(); err != nil {
		return err
	}
	return parseValues(r.PostForm, dst)
}

func parseURLParams(r *http.Request, dst interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}
	return parseValues(r.Form, dst)
}

func parseValues(values url.Values, dst interface{}) error {
	dec := schema.NewDecoder()
	// Used for CSRF Keys to pass
	dec.IgnoreUnknownKeys(true)
	//Decoder recieve a pointer to a dst struct(SignupForm)
	//and transforms map sting[]strings(PostForm/Form) to struct
	if err := dec.Decode(dst, values); err != nil {
		return err
	}
	return nil
}
