package controllers

import (
	"net/http"
	"time"

	"lenslocked.com/context"
	"lenslocked.com/email"
	"lenslocked.com/models"
	"lenslocked.com/rand"
	"lenslocked.com/views"
)

// NewUsers is used to create a new Users controller.
// This function will panic if the tempaltes are not
// parsed correclty, and should only be used during
// initial setup.
func NewUsers(us models.UserService, emailer *email.Client) *Users {
	return &Users{
		NewView:      views.NewView("bootstrap", "users/new"),
		LoginView:    views.NewView("bootstrap", "users/login"),
		ForgotPwView: views.NewView("bootstrap", "users/forgot_pw"),
		ResetPwView:  views.NewView("bootstrap", "users/reset_pw"),
		us:           us,
		emailer:      emailer,
	}
}

type Users struct {
	NewView      *views.View
	LoginView    *views.View
	ForgotPwView *views.View
	ResetPwView  *views.View
	us           models.UserService
	emailer      *email.Client
}

// New is used for users struct to render the new view (signup form)
// so a user can create an account.
//
// GET /signup
func (u *Users) New(w http.ResponseWriter, r *http.Request) {
	var form SignupForm
	parseURLParams(r, &form)
	// form data will get converted to yd
	// ex /signup?name=Jon%20Smith&email=jon@smith.com will populate name and
	// email fields
	u.NewView.Render(w, r, form)
}

type SignupForm struct {
	// schema matches name field in html
	Name     string `schema:"name"`
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// Create is used for users struct to process the signup form when a
// user submits it. This is used to create a new user account.
//
// POST /signup
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form SignupForm
	// form pointer is put into view data so the data can persist incase of an error
	// or a redirect back to the signup page
	vd.Yield = &form
	// data is taken from request pointer and decoded into form struct
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		u.NewView.Render(w, r, vd)
		return
	}
	user := models.User{
		Name:     form.Name,
		Email:    form.Email,
		Password: form.Password,
	}
	if err := u.us.Create(&user); err != nil {
		vd.SetAlert(err)
		u.NewView.Render(w, r, vd)
		return
	}
	// At this point Password is removed and the PasswordHash, Remember, and
	// Remember hash are set
	//
	// Send welcome email to new user after create
	u.emailer.Welcome(user.Name, user.Email)

	// Passing in user as ptr as user in this func is not a ptr
	err := u.signIn(w, &user)
	if err != nil {
		// Redirect if cookie set failure
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	// Craft alert to pass into the redirect
	alert := views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Welcome " + user.Name,
	}
	// After sucessful creation redirect to /galleries
	views.RedirectAlert(w, r, "/galleries", http.StatusFound, alert)

}

type LoginForm struct {
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// Login is used to verify the provivded email address and password
// and then log the user in if they are correct.
// Post /login
func (u *Users) Login(w http.ResponseWriter, r *http.Request) {
	vd := views.Data{}
	form := LoginForm{}
	// If the form cannot be parsed log/set error/alert and render the login view.
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		u.LoginView.Render(w, r, vd)
		return
	}
	user, err := u.us.Authenticate(form.Email, form.Password)
	if err != nil {
		switch err {
		case models.ErrNotFound:
			vd.AlertError("Invalid email address")
		default:
			vd.SetAlert(err)
		}
		u.LoginView.Render(w, r, vd)
		return
	}
	// User is passed in as is as because a ptr is returned by the Authenticate
	// method
	err = u.signIn(w, user)
	if err != nil {
		vd.SetAlert(err)
		u.LoginView.Render(w, r, vd)
		return
	}
	// Craft alert to pass into the redirect
	alert := views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Welcome " + user.Name,
	}
	// After sucessful login redirect to /galleries
	views.RedirectAlert(w, r, "/galleries", http.StatusFound, alert)
}

// Logout is used to delete a users session cookie (remember_token) and then
// will update the user resource with a new remember token.
//
// POST /logout
func (u *Users) Logout(w http.ResponseWriter, r *http.Request) {
	// Invalidate remember token cookie
	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    "",
		Expires:  time.Now(),
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	user := context.User(r.Context())
	// Set a new remember token for the user
	token, _ := rand.RememberToken()
	user.Remember = token
	u.us.Update(user)
	http.Redirect(w, r, "/", http.StatusFound)
}

// ResetPwForm is used to process the forgot pasword form
// and reset password form

type ResetPwForm struct {
	Email    string `schema:"email"`
	Token    string `schema:"token"`
	Password string `schema:"password"`
}

// POST /forgot
func (u *Users) InitiateReset(w http.ResponseWriter, r *http.Request) {
	// TODO: process the forgot password form and initiate that process
	var vd views.Data
	var form ResetPwForm
	vd.Yield = &form
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		u.ForgotPwView.Render(w, r, vd)
		return
	}
	token, err := u.us.InitiateReset(form.Email)
	if err != nil {
		vd.SetAlert(err)
		u.ForgotPwView.Render(w, r, vd)
		return
	}
	// Email the user the token and reset link
	err = u.emailer.ResetPw(form.Email, token)
	if err != nil {
		vd.SetAlert(err)
		u.ForgotPwView.Render(w, r, vd)
		return
	}

	views.RedirectAlert(w, r, "/reset", http.StatusFound, views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Instructions for resetting your password have been emailed to you.",
	})
}

// ResetPw displays the reset password form and has a method so that we can
// prefill the form data with the token provided via the URL query params
// GET /reset
func (u *Users) ResetPw(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form ResetPwForm
	vd.Yield = &form
	// Parse URL Params into form struct
	if err := parseURLParams(r, &form); err != nil {
		vd.SetAlert(err)
		u.ResetPwView.Render(w, r, vd)
		return
	}
	// Render the view with the form data prefilled from the parse
	u.ResetPwView.Render(w, r, vd)
}

// CompleteReset processes the reset password form
// POST /reset
func (u *Users) CompleteReset(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form ResetPwForm
	vd.Yield = &form
	// Populate ResetPwForm from html form
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		u.ResetPwView.Render(w, r, vd)
		return
	}
	// Initiate reset
	user, err := u.us.CompleteReset(form.Token, form.Password)
	if err != nil {
		vd.SetAlert(err)
		u.ResetPwView.Render(w, r, vd)
		return
	}
	// Set cookies
	u.signIn(w, user)
	views.RedirectAlert(w, r, "/galleries", http.StatusFound, views.Alert{
		Level:   views.AlertLvlSuccess,
		Message: "Your password has been reset and you have been logged in!",
	})
}

// signIn is used to sign the given user in via cookies
// If a Remember token does not exist one is generated
// Update will also take care of hashing the Remember token and updating the DB
// Finally the cookie is set.
func (u *Users) signIn(w http.ResponseWriter, user *models.User) error {
	if user.Remember == "" {
		token, err := rand.RememberToken()
		if err != nil {
			return err
		}
		user.Remember = token
		err = u.us.Update(user)
		if err != nil {
			return err
		}
	}

	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    user.Remember,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	return nil
}
