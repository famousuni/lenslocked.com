package views

import (
	"bytes"
	"errors"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"path/filepath"

	"github.com/gorilla/csrf"
	"lenslocked.com/context"
)

var (
	LayoutDir   string = "views/layouts/"
	TemplateDir string = "views/"
	TemplateExt string = ".gohtml"
)

func NewView(layout string, files ...string) *View {
	addTemplatePath(files)
	addTemplateExt(files)
	files = append(files, layoutFiles()...)
	t, err := template.New("").Funcs(template.FuncMap{
		"csrfField": func() (template.HTML, error) {
			return "", errors.New("csrfField is not implemented")
		},
		"pathEscape": func(s string) string {
			return url.PathEscape(s)
		},
	}).ParseFiles(files...)
	if err != nil {
		panic(err)
	}

	return &View{
		Template: t,
		Layout:   layout,
	}
}

type View struct {
	Template *template.Template
	Layout   string
}

func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	v.Render(w, r, nil)
}

// Render is used to render the view with the predefined layout.
func (v *View) Render(w http.ResponseWriter, r *http.Request, data interface{}) {
	w.Header().Set("Content-Type", "text/html")
	var vd Data
	// type switch
	// Check if data var is of type Data, if true store it in vd otherwise store it
	// as yield data within a new Data struct in vd.
	switch d := data.(type) {
	case Data:
		vd = d
	default:
		vd = Data{
			Yield: data,
		}
	}
	// Check for an Alert set in the request cookies. If this exists and there is
	// not a genuine Alert from another error then set the Alert within the template.
	if alert := getAlert(r); alert != nil && vd.Alert == nil {
		vd.Alert = alert
		// Remove alert cookies
		clearAlert(w)
	}
	vd.User = context.User(r.Context())
	var buf bytes.Buffer
	// Document CSRF Flow
	csrfField := csrf.TemplateField(r)
	tpl := v.Template.Funcs(template.FuncMap{
		"csrfField": func() template.HTML {
			return csrfField
		},
	})

	if err := tpl.ExecuteTemplate(&buf, v.Layout, vd); err != nil {
		log.Println(err)
		http.Error(w, "Something went wrong. If the problem persists, please email support@slenslocked.com", http.StatusInternalServerError)
		return
	}
	io.Copy(w, &buf)
}

// layoutFiles returns a slice of strings representing
// the layout files used in our application.
// This adds thigns such as footer and navbar files.
func layoutFiles() []string {
	files, err := filepath.Glob(LayoutDir + "*" + TemplateExt)
	if err != nil {
		panic(err)
	}
	return files
}

//addTemplatePath takes in a slice of strings represending
//file paths for templates, and it prepends the TemplateDir
//directory to each string in a slice
//
//Eg the input {"home"} woudl result in the output
//{"views/home"} if the TemplateDir == "views"
func addTemplatePath(files []string) {
	for i, f := range files {
		files[i] = TemplateDir + f
	}
}

//addTemplateExt take in a slice of strings
//representing file paths for templates and it appends
//the TemplateExt extension to each string in the slice
//
//eg the input {"home"} would result in the output
//{"home.gohtml"} if the TemplateExt = ".gohtml"

func addTemplateExt(files []string) {
	for i, f := range files {
		files[i] = f + TemplateExt
	}
}
