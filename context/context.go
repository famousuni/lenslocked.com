package context

import (
	"context"

	"lenslocked.com/models"
)

const (
	// userkey is cast to privateKey in order to prevent other controllers
	// from overwritting it in the context with a string.
	userKey privateKey = "user"
)

type privateKey string

// WithUser will take in a context and models.User pointer and set the obj
// within the context and return the updated context
func WithUser(ctx context.Context, user *models.User) context.Context {
	return context.WithValue(ctx, userKey, user)
}

// User will take in a context and return the models.User obj from the context
// if it contains one
func User(ctx context.Context) *models.User {
	// Check if context has the User key
	if temp := ctx.Value(userKey); temp != nil {
		// Type conversion to convert interface to models.User pointer
		if user, ok := temp.(*models.User); ok {
			return user
		}
	}
	// Otherwise return nil
	return nil
}
