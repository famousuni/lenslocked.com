package models

import "strings"

const (
	// ErrNotFound is returned when a resource cannot be ErrorNotFound
	// in the database.
	ErrNotFound modelError = "models: resource not found"
	// ErrPasswordIncorrect is returend when an invalid password is used when
	// attempting to authenticate a user
	ErrPasswordIncorrect modelError = "models: incorrect password provided"
	// ErrEmailRequired is returned when an email address is not provided
	// when creating a user
	ErrEmailRequired modelError = "models: Email address is required"
	// ErrEmailInvalid is returned when an email address provided does not
	// match any of our requirements
	ErrEmailInvalid modelError = "models: Email address is not valid"
	// ErrEmailTaken is returned when an update or create is attempted
	// with an email address that is alreayd in use.
	ErrEmailTaken modelError = "models: email address is already taken"
	// ErrPasswordTooShort is returned when an update or create is attempted
	// with a user password that is less than 8 characters.
	ErrPasswordTooShort modelError = "models: password must be at least 8 characters long"
	// ErrPasswordRequired is returned when a create is attempted without a user
	// password provided.
	ErrPasswordRequired modelError = "models: password is requried"
	// ErrTitleRequired is returned if a title is not provided when creating a gallery
	ErrTitleRequired modelError = "models: title is required"
	// ErrTokenInvalid is returned of the pwReset token is expired
	ErrTokenInvalid modelError = "models: token provided is not valid"
	// ErrRememberTooShort is returned when a remember token is not at least 32 bytes.
	ErrRememberTooShort privateError = "models: remember token must be at least 32 bytes"
	// ErrRememberRequired is returned when a create or update is attempted without
	// a valid user remember token hash.
	ErrRememberRequired privateError = "models: remember token is required."
	// ErrUserIDRequired is returned if the User ID is not provided when creating a gallery
	// This should not be returned to the user so it is cast to privateError
	ErrUserIDRequired privateError = "models: user ID is required"
	// ErrIDInvalid is returned when an invalid ID is provided
	// to a method like Delete
	ErrIDInvalid privateError = "models: ID provided was invalid"
)

type modelError string

// Error is used to return the modelError const as a string
func (e modelError) Error() string {
	return string(e)
}

// Public is used in SetAlert in order to present a sanitized error to the client
// Right now it removes the models: prefix and title cases the string
func (e modelError) Public() string {
	s := strings.Replace(string(e), "models: ", "", 1)
	split := strings.Split(s, " ")
	split[0] = strings.Title(split[0])
	return strings.Join(split, " ")
	//return strings.Title(s)
}

// privateError does not implement Public() so these errors will be masked
// when presented to the client.
type privateError string

func (e privateError) Error() string {
	return string(e)
}
