package models

import (
	"fmt"
	"io"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

// Image is NOT stored in the database
type Image struct {
	GalleryID uint
	Filename  string
}

// Used to build the Image struct
func (i *Image) Path() string {
	// URL conversion will encode characters incase special characters were
	// returned by RelativePath such as filename
	temp := url.URL{
		Path: "/" + i.RelativePath(),
	}
	// String conversion allows the return
	return temp.String()
}

// Used in Delete calls
func (i *Image) RelativePath() string {
	return fmt.Sprintf("images/galleries/%v/%v", i.GalleryID, i.Filename)
}

type ImageService interface {
	Create(galleryID uint, r io.ReadCloser, filename string) error
	ByGalleryID(galleryID uint) ([]Image, error)
	Delete(i *Image) error
}

func NewImageService() ImageService {
	return &imageService{}
}

type imageService struct{}

func (is *imageService) Create(galleryID uint, r io.ReadCloser, filename string) error {
	defer r.Close()
	path, err := is.mkimagePath(galleryID)
	if err != nil {
		return err
	}
	// Create a destination file
	dst, err := os.Create(path + filename)
	if err != nil {
		return err
	}
	defer dst.Close()
	// Copy reader data to destination file
	_, err = io.Copy(dst, r)
	if err != nil {
		return err
	}
	return nil
}

func (is *imageService) ByGalleryID(galleryID uint) ([]Image, error) {
	path := is.imagePath(galleryID)
	imgStrings, err := filepath.Glob(path + "*")
	if err != nil {
		return nil, err
	}
	// Create a slice of images equal to the amount of strings returned
	ret := make([]Image, len(imgStrings))
	for i := range imgStrings {
		// Strip off path so we are left with only the image filename
		imgStrings[i] = strings.Replace(imgStrings[i], path, "", 1)
		// For each slice of imgStrings create an Image obj in the ret slice
		ret[i] = Image{
			Filename:  imgStrings[i],
			GalleryID: galleryID,
		}
	}
	return ret, nil
}

func (is *imageService) Delete(i *Image) error {
	return os.Remove(i.RelativePath())
}

func (is *imageService) imagePath(galleryID uint) string {
	return fmt.Sprintf("images/galleries/%v/", galleryID)
}

func (is *imageService) mkimagePath(galleryID uint) (string, error) {
	// Create the gallery path string from the gallery ID
	galleryPath := is.imagePath(galleryID)
	// Create the folder
	err := os.MkdirAll(galleryPath, 0755)
	if err != nil {
		return "", err
	}
	return galleryPath, nil
}
