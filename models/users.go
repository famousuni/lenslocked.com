package models

import (
	"regexp"
	"strings"
	"time"

	"github.com/jinzhu/gorm"

	"lenslocked.com/hash"
	"lenslocked.com/rand"

	"golang.org/x/crypto/bcrypt"
)

// User holds all the relevant user information
type User struct {
	gorm.Model
	Name         string
	Email        string `gorm:"not null;unique_index"`
	Password     string `gorm:"-"`
	PasswordHash string `gorm:"not null"`
	Remember     string `gorm:"-"`
	RememberHash string `gorm:"not null;unique_index"`
}

// UserDB is used to interact with the users database.
//
// For pretty much all single user queries:
// 1 - user, nil
// 2 - nil, ErrNotFound
// 3 - nil, otherError
//
// For single user queries, any error but ErrNotFound should
// probably result in a 500 error.
type UserDB interface {
	// Methods for querying for single users
	ByID(id uint) (*User, error)
	ByEmail(email string) (*User, error)
	ByRemember(token string) (*User, error)
	// Methods for altering users
	Create(user *User) error
	Update(user *User) error
	Delete(id uint) error
}

// UserService is a set of methods used to manipulate and
// work with the user model
type UserService interface {
	// Authenticate will verify the provided email address and
	// password are correct. If they are correct, the user
	// corresponding to that email will be returned. Otherwise
	// You will recieve either:
	// ErrNotFound, ErrPasswordIncorrect, or another error if
	// something goes wrong.
	Authenticate(email, password string) (*User, error)
	// InitiateReset will start the reset password process
	// by creating the reset token for the user found with the
	// provided email address
	InitiateReset(email string) (string, error)
	CompleteReset(token, newPw string) (*User, error)
	UserDB
}

var _ UserService = &userService{}

func NewUserService(db *gorm.DB, pepper, hmacKey string) UserService {
	ug := &userGorm{db}
	hmac := hash.NewHMAC(hmacKey)
	uv := newUserValidator(ug, hmac, pepper)
	return &userService{
		UserDB: uv,
		pepper: pepper,
		// gormdb is passed into pwResetGorm within newPwResetValidator
		pwResetDB: newPwResetValidator(&pwResetGorm{db}, hmac),
	}
}

type userService struct {
	UserDB
	pepper    string
	pwResetDB pwResetDB
}

//Authenticate can be used to authenticte a user with the
//provided email address and password.
// If the email address provided is invalid this will return
// nil, ErrNotFound
// If the password provided is invalid, this will return
// nil, ErrPasswordIncorrect
// If the email and password are both valid, this will return
// User, nil
// Otherwise if another error is encountered this will return
// nil, error
func (us *userService) Authenticate(email, password string) (*User, error) {
	// ByEmail will call ByEmail in the UserDB of the userService which is
	// userValidator. The ByEmail method of userValidator will normalize the email
	// before calling the ByEmail method of userGorm
	foundUser, err := us.ByEmail(email)
	if err != nil {
		return nil, err
	}
	err = bcrypt.CompareHashAndPassword([]byte(foundUser.PasswordHash), []byte(password+us.pepper))
	if err != nil {
		switch err {
		case bcrypt.ErrMismatchedHashAndPassword:
			return nil, ErrPasswordIncorrect
		default:
			return nil, err
		}
	}
	return foundUser, nil
}

// InitiateReset will take in an email, create the pwReset obj for the user
// and return a reset token
func (us *userService) InitiateReset(email string) (string, error) {
	// Lookup user by email
	user, err := us.ByEmail(email)
	if err != nil {
		return "", err
	}
	// Create pwReset using the userID
	pwr := pwReset{
		UserID: user.ID,
	}
	if err := us.pwResetDB.Create(&pwr); err != nil {
		return "", err
	}
	// If sucessful return the pwReset token for the user
	return pwr.Token, nil
}

// CompleteReset takes in a token and new password string. It will then
// lookup the pwReset obj by the token and make sure the token is < 12 hours
// old. It will then lookup the user by the ID tied to the pwReset obj and
// update user.password with the newPW. Finally it will update the user in the
// user table, delete the pwReset obj and return the user.
func (us *userService) CompleteReset(token, newPw string) (*User, error) {
	// Lookup a pwReset using the token
	pwr, err := us.pwResetDB.ByToken(token)
	if err != nil {
		if err == ErrNotFound {
			return nil, ErrTokenInvalid
		}
		return nil, err
	}
	// Make sure the token is valid (not > 12 hrs old)
	if time.Now().Sub(pwr.CreatedAt) > (12 * time.Hour) {
		return nil, ErrTokenInvalid
	}
	// Lookup the user by the pwReset.UserID
	user, err := us.ByID(pwr.UserID)
	if err != nil {
		return nil, err
	}
	// Update the user's password w/ the new password
	user.Password = newPw
	err = us.Update(user)
	if err != nil {
		return nil, err
	}
	// Delete the pwReset
	us.pwResetDB.Delete(pwr.ID)
	// Return updated user and nil error
	return user, nil
}

var _ UserDB = &userValidator{}

// newUserValidator
func newUserValidator(udb UserDB, hmac hash.HMAC, pepper string) *userValidator {
	return &userValidator{
		UserDB:     udb,
		hmac:       hmac,
		emailRegex: regexp.MustCompile(`^[a-z0-9._%+\-]+@` + `[a-z0-9.\-]+\.[a-z]{2,16}$`),
		pepper:     pepper,
	}
}

type userValidator struct {
	UserDB
	hmac       hash.HMAC
	emailRegex *regexp.Regexp
	pepper     string
}

// ByEmail will normalize the email address before calling
// ByEmail on the UserDB field.
func (uv *userValidator) ByEmail(email string) (*User, error) {
	user := User{
		Email: email,
	}
	if err := runUserValFuncs(&user, uv.normalizeEmail); err != nil {
		return nil, err
	}
	return uv.UserDB.ByEmail(user.Email)
}

// ByRemember will hash the remember token and then call
// ByRemember on the subsequent UserDB layer.
func (uv *userValidator) ByRemember(token string) (*User, error) {
	// Create the user struct to pass into uv.hmacRemember
	// uv.hmacRemember will set user.RememberHash
	user := User{
		Remember: token,
	}
	if err := runUserValFuncs(&user, uv.hmacRemember); err != nil {
		return nil, err
	}
	return uv.UserDB.ByRemember(user.RememberHash)
}

// Create will run validators and call Create from UserGorm
func (uv *userValidator) Create(user *User) error {
	//runUserValFuncs is called to iterate over validation functions
	//passowrd Required verifies if user.password is an empty string.
	//passwordMinLength verifies user.password is at least 8 characters.
	//bcyrptPassword bcrypts the pw and sets it in Password Hash field and sets Password to ""
	//passwordHashRequired verifies that user.passwordhash is set
	//defaultRemember will set a remember token if user.Remember is equal to ""
	//rememberMinBytes checks the remember token is 32 bytes.
	//hmacRemember hashs the remember token and sets the hash value to RememberHash
	//rememberHashRequired verifies that user remember hash is set.
	//normalizeEmail will convert all characters to lowercase and strip white space
	//requireEmail checks if user.email is an empty string and returns an error
	//eamilFormat checks the email against emailRegex
	//emailIsAvailable checks if the user is in the DB and if the id matches
	err := runUserValFuncs(user,
		uv.passwordRequired,
		uv.passwordMinLength,
		uv.bcryptPassword,
		uv.passwordHashRequired,
		uv.defaultRemember,
		uv.rememberMinBytes,
		uv.hmacRemember,
		uv.rememberHashRequired,
		uv.normalizeEmail,
		uv.requireEmail,
		uv.emailFormat,
		uv.emailIsAvailable)
	if err != nil {
		return err
	}
	return uv.UserDB.Create(user)
}

// Update will update the User entry in the DB
func (uv *userValidator) Update(user *User) error {
	//runUserValFuncs is called to iterate over validation functions
	err := runUserValFuncs(user,
		uv.passwordMinLength,
		uv.bcryptPassword,
		uv.passwordHashRequired,
		uv.rememberMinBytes,
		uv.hmacRemember,
		uv.rememberHashRequired,
		uv.normalizeEmail,
		uv.requireEmail,
		uv.emailFormat,
		uv.emailIsAvailable)
	if err != nil {
		return err
	}
	return uv.UserDB.Update(user)
}

// Delete will delete the user with the provided ID
func (uv *userValidator) Delete(id uint) error {
	var user User
	user.ID = id
	// uv.idGreaterThan is passed in as it returns a function of userValFunc type
	// which is then ran by runUserValFuncs()
	err := runUserValFuncs(&user, uv.idGreaterThan(0))
	if err != nil {
		return err
	}
	return uv.UserDB.Delete(id)
}

// userValFunc type is created in order to pass in multiple validation functions
// to runUserValFuncs
type userValFunc func(*User) error

// runUserValFuncs purpose is to take in 1 or more functions of type userValFunc
// iterate over them and return errors if any
func runUserValFuncs(user *User, fns ...userValFunc) error {
	for _, fn := range fns {
		if err := fn(user); err != nil {
			return err
		}
	}
	return nil
}

// bcryptPassowrd is a method of userValidator struct that also is of type
// userValFunc. This method takes in a user pointer, hashes user.password with a
// predefined pepper (userPwPepper) and bcrypt if the user.Password is not empty.
func (uv *userValidator) bcryptPassword(user *User) error {
	// If password is empty string return nil otherwise hash
	// the password.
	if user.Password == "" {
		return nil
	}
	//Add pepper to posted password and convert to byte slice
	pwBytes := []byte(user.Password + uv.pepper)
	//Hash peppered password
	hashedBytes, err := bcrypt.GenerateFromPassword(pwBytes, bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.PasswordHash = string(hashedBytes)
	user.Password = ""
	return nil
}

// hmacRemember is a method of userValidator struct that also is of type
// userValFunc. This method takes in a user pointer, hashes the remember token
// and sets user.RememberHash to the hash value
func (uv *userValidator) hmacRemember(user *User) error {
	if user.Remember == "" {
		return nil
	}
	user.RememberHash = uv.hmac.Hash(user.Remember)
	return nil
}

// defaultRemember sets generates a token and sets the value to
// user.Remember if user.Remember is ""
func (uv *userValidator) defaultRemember(user *User) error {
	if user.Remember != "" {
		return nil
	}
	token, err := rand.RememberToken()
	if err != nil {
		return err
	}
	user.Remember = token
	return nil
}

// rememberMinBytes verifies if the remember token is at least 32 bytes.
func (uv *userValidator) rememberMinBytes(user *User) error {
	if user.Remember == "" {
		return nil
	}
	n, err := rand.NBytes(user.Remember)
	if err != nil {
		return err
	}
	if n < 32 {
		return ErrRememberTooShort
	}
	return nil
}

// rememberHashRequired verifies if user.RememberHash is set.
func (uv *userValidator) rememberHashRequired(user *User) error {
	if user.RememberHash == "" {
		return ErrRememberRequired
	}
	return nil
}

// idGreaterThan returns a userValFunc that checks can check if user.ID is
// less than n.
func (uv *userValidator) idGreaterThan(n uint) userValFunc {
	return userValFunc(func(user *User) error {
		if user.ID <= n {
			return ErrIDInvalid
		}
		return nil
	})
}

// normalizeEmail will take user.Email and make all chars lowercase.
// It will then trim all whitespace.
func (uv *userValidator) normalizeEmail(user *User) error {
	user.Email = strings.ToLower(user.Email)
	user.Email = strings.TrimSpace(user.Email)
	return nil
}

// requireEmail checks if user.Email is an empty string and returns an
// an error.
func (uv *userValidator) requireEmail(user *User) error {
	if user.Email == "" {
		return ErrEmailRequired
	}
	return nil
}

// emailFormat checks the email string against the emailRegex var
func (uv *userValidator) emailFormat(user *User) error {
	if !uv.emailRegex.MatchString(user.Email) {
		return ErrEmailInvalid
	}
	return nil
}

// emailIsAvailable checks if the email is in the db and compares the
// retrieved user id with the user struct user id.
func (uv *userValidator) emailIsAvailable(user *User) error {
	existing, err := uv.ByEmail(user.Email)
	if err == ErrNotFound {
		// Email address is not taken
		return nil
	}
	if err != nil {
		return err
	}
	// We found a user with this email address
	// If the found user has the same ID as this user, it is
	// an update and this is the same user.
	if user.ID != existing.ID {
		return ErrEmailTaken
	}
	return nil
}

// passwordMinLength verifies if the password is greater than 8 characters.
// If it is less than 8 it returns ErrPasswordTooShort
func (uv *userValidator) passwordMinLength(user *User) error {
	if user.Password == "" {
		return nil
	}
	if len(user.Password) < 8 {
		return ErrPasswordTooShort
	}
	return nil
}

// passwordRequired verififes that the passwork is not an empty string.
// If it is an empty string it will return ErrPasswordRequired.
func (uv *userValidator) passwordRequired(user *User) error {
	if user.Password == "" {
		return ErrPasswordRequired
	}
	return nil
}

// passwordHashRequired verifieds if the password hash is not an empty string.
// If it is an empty string it will return ErrPasswordRequired.
func (uv *userValidator) passwordHashRequired(user *User) error {
	if user.PasswordHash == "" {
		return ErrPasswordRequired
	}
	return nil
}

var _ UserDB = &userGorm{}

type userGorm struct {
	db *gorm.DB
}

// ByID will look up by the ID provided.
// 1 - user, nil
// 2 - nil, ErrNotFound
// 3 - nil, otherError
// As a general rule, any error but ErrNotFound should
// probably result in a 500 error.
func (ug *userGorm) ByID(id uint) (*User, error) {
	var user User
	db := ug.db.Where("id = ?", id)
	err := first(db, &user)
	return &user, err
}

// ByEmail looks up a user with the given email address and
// returns that user
// 1 - user, nil
// 2 - nil, ErrNotFound
// 3 - nil, otherError
// As a general rule, any error but ErrNotFound should
// probably result in a 500 error.
func (ug *userGorm) ByEmail(email string) (*User, error) {
	var user User
	db := ug.db.Where("email = ?", email)
	err := first(db, &user)
	return &user, err
}

// ByRemember looks up a user with the given remember token
// and returns that user. This method expects the remember
// token to already be hashed.
// Error are the same as ByEmail.
func (ug *userGorm) ByRemember(rememberHash string) (*User, error) {
	var user User
	err := first(ug.db.Where("remember_hash = ?", rememberHash), &user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// first will query using the provided gorm.DB and it will
// get the first item returned and place it into dst. If
// nothing is found in the query it will return ErrNotFound
func first(db *gorm.DB, dst interface{}) error {
	err := db.First(dst).Error
	if err == gorm.ErrRecordNotFound {
		return ErrNotFound
	}
	return err
}

// Create will create the provided user and backfill data
// like the ID, CreatedAt, and UpdatedAt fields.
func (ug *userGorm) Create(user *User) error {
	return ug.db.Create(user).Error
}

// Update will update the User entry in the DB
func (ug *userGorm) Update(user *User) error {
	return ug.db.Save(user).Error
}

//Delete will delete the user with the provided ID
func (ug *userGorm) Delete(id uint) error {
	user := User{Model: gorm.Model{ID: id}}
	return ug.db.Delete(&user).Error
}
