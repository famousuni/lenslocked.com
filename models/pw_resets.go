package models

import (
	"github.com/jinzhu/gorm"
	"lenslocked.com/hash"
	"lenslocked.com/rand"
)

// Struct for our pwReset table/objs
type pwReset struct {
	gorm.Model
	UserID    uint   `gorm:"not null"`
	Token     string `gorm:"-"`
	TokenHash string `gorm:"not null;unique_index"`
}

type pwResetDB interface {
	ByToken(token string) (*pwReset, error)
	Create(pwr *pwReset) error
	Delete(id uint) error
}

// used to build the pwResetValidtor
func newPwResetValidator(db pwResetDB, hmac hash.HMAC) *pwResetValidator {
	return &pwResetValidator{
		pwResetDB: db,
		hmac:      hmac,
	}
}

type pwResetValidator struct {
	pwResetDB
	hmac hash.HMAC
}

// ByToken in pwrv will set the token of the pwReset struct to the passed in Token
// it will then hash the token and return the pwReset struct that matches the Token hash
func (pwrv *pwResetValidator) ByToken(token string) (*pwReset, error) {
	pwr := pwReset{Token: token}
	err := runPwResetValFns(&pwr, pwrv.hmacToken)
	if err != nil {
		return nil, err
	}
	return pwrv.pwResetDB.ByToken(pwr.TokenHash)
}

// Create in pwrv check the user id is >= 0, set a token if it is an empty string,
// and hash the token and store the hash in pwr struct. Finally it will create the
// pwr obj in the DB
func (pwrv *pwResetValidator) Create(pwr *pwReset) error {
	err := runPwResetValFns(pwr,
		pwrv.requireUserID,
		pwrv.setTokenIfUnset,
		pwrv.hmacToken,
	)
	if err != nil {
		return err
	}
	return pwrv.pwResetDB.Create(pwr)
}

// Delete in pwrv will check if the id <= 0 if this check passes it will call
// Delete in the pwResetDB to delete the pwr obj from the DB
func (pwrv *pwResetValidator) Delete(id uint) error {
	if id <= 0 {
		return ErrIDInvalid
	}
	return pwrv.pwResetDB.Delete(id)
}

type pwResetValFn func(*pwReset) error

// RunPwResetValFns takes in 1 or more pwResetValFns and applies each to
// the pwReset struct pointer
func runPwResetValFns(pwr *pwReset, fns ...pwResetValFn) error {
	for _, fn := range fns {
		if err := fn(pwr); err != nil {
			return err
		}
	}
	return nil
}

func (pwrv *pwResetValidator) requireUserID(pwr *pwReset) error {
	if pwr.UserID <= 0 {
		return ErrUserIDRequired
	}
	return nil
}

func (pwrv *pwResetValidator) setTokenIfUnset(pwr *pwReset) error {
	if pwr.Token != "" {
		return nil
	}
	token, err := rand.RememberToken()
	if err != nil {
		return err
	}
	pwr.Token = token
	return nil
}

func (pwrv *pwResetValidator) hmacToken(pwr *pwReset) error {
	if pwr.Token == "" {
		return nil
	}
	pwr.TokenHash = pwrv.hmac.Hash(pwr.Token)
	return nil
}

type pwResetGorm struct {
	db *gorm.DB
}

// ByToken will take in a tokenHash and return the pwReset obj that matches
// the token hash
func (pwrg *pwResetGorm) ByToken(tokenHash string) (*pwReset, error) {
	var pwr pwReset
	err := first(pwrg.db.Where("token_hash = ?", tokenHash), &pwr)
	if err != nil {
		return nil, err
	}
	return &pwr, nil
}

// Create will create a pwReset obj within the DB
func (pwrg *pwResetGorm) Create(pwr *pwReset) error {
	return pwrg.db.Create(pwr).Error
}

// Delete will delete the pwReset obj within the DB
func (pwrg *pwResetGorm) Delete(id uint) error {
	// Craft pwReset obj with passed in id as ID
	pwr := pwReset{Model: gorm.Model{ID: id}}
	// Delete item by ID from pwReset table
	return pwrg.db.Delete(&pwr).Error
}
