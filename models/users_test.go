package models

import (
	"fmt"
	"testing"
	"time"
)

func testingUserService() (UserService, error) {
	const (
		host     = "localhost"
		port     = 5432
		user     = "postgres"
		password = "your-password"
		dbname   = "lenslocked_test"
	)
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable", host, port, user, dbname)
	//services, err := NewServices("postgres", psqlInfo)

	services, err := NewServices(
		WithGorm("postgres", psqlInfo),
		WithLogMode(true),
		WithUser("secret-random-string", "secret-hmac-key"),
		WithGallery(),
		WithImage(),
	)

	//us := NewUserService(psqlInfo)
	if err != nil {
		return nil, err
	}
	// Clear the users table between tests
	services.DestructiveReset()
	return services.User, nil
}

func TestCreateUser(t *testing.T) {
	us, err := testingUserService()
	if err != nil {
		t.Fatal(err)
	}
	user := User{
		Name:     "Michael Scott",
		Email:    "michael@dundermifflin.com",
		Password: "office123",
	}
	err = us.Create(&user)
	if err != nil {
		t.Fatal(err)
	}
	if user.ID == 0 {
		t.Errorf("Expected ID > 0. Received %d", user.ID)
	}
	if time.Since(user.CreatedAt) > time.Duration(5*time.Second) {
		t.Errorf("Expected CreatedAt to be recent. Received %s", user.CreatedAt)
	}
	if time.Since(user.UpdatedAt) > time.Duration(5*time.Second) {
		t.Errorf("Expected UpdatedAt to be recent. Received %s", user.CreatedAt)
	}
}
