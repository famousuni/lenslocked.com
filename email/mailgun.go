package email

import (
	"fmt"
	"net/url"

	mailgun "gopkg.in/mailgun/mailgun-go.v1"
)

const (
	welcomeSubject = "Welcome to LensLocked.com!"

	resetSubject = "Instructions for resetting your password."

	resetBaseURL = "https://www.lenslocked.com/reset"

	welcomeText = `Hi There Welcome to LensLocked.com! We really hope you enjoy using our application!

  Best,
  Charlie
  `
	welcomeHTML = `Hi there!<br/>
  <br/>
  Welcome to
  <a href="https://www.lenslocked.com">LensLocked.com</a>! We really hope you enjoy using our application!<br/>
  <br/>
  Best,<br/>
  Charlie
  `

	resetTextTmpl = `Hi there!

	It appears that you have requested a password reset. If this was you, please follow the link
	below to update your password:

	%s

	If you are asked for a token, please use the following value:

	%s

	If you didnt request a password reset you can safely ignore this email and your account
	will not be changed.

	Best,
	Charlie
	`

	resetHTMLTmpl = `Hi there!<br/>
	<br/>
	It appears that you have requested a password reset. If this was you, please follow the link
	below to update your password:
	<br/>
	<a href="%s">%s</a><br/>
	<br/>
	If you are asked for a token, please use the following value:
	<br/>
	%s<br/>
	<br/>
	If you didnt request a password reset you can safely ignore this email and your account
	will not be changed.
	<br/>
	Best,<br/>
	Charlie<br/>
	`
)

// WithMailgun returns a ClientConfig func that crafts the mailgun client and
// sets it within the Client struct
func WithMailgun(domain, apiKey, publicKey string) ClientConfig {
	return func(c *Client) {
		mg := mailgun.NewMailgun(domain, apiKey, publicKey)
		c.mg = mg
	}
}

// WithSender returns a ClientConfig func that sets the sender within the Client
// struct
func WithSender(name, email string) ClientConfig {
	return func(c *Client) {
		c.from = buildEmail(name, email)
	}
}

// ClientConfig type is a function that takes in a Client pointer
type ClientConfig func(*Client)

// NewClient will take in 1 or more ClientConfig functions and apply them to
// the a Client struct and return a pointer to the Client struct
func NewClient(opts ...ClientConfig) *Client {
	client := Client{
		// Set a default from email address....
		from: "support@lenslocked.com",
	}
	for _, opt := range opts {
		opt(&client)
	}
	return &client
}

// Client is a struct that contains the from email address and a mailgun client obj
type Client struct {
	from string
	mg   mailgun.Mailgun
}

// Welcome will take in a name and email and craft a default signup email. It will
// then send the email to the toEmail
func (c *Client) Welcome(toName, toEmail string) error {
	message := mailgun.NewMessage(c.from, welcomeSubject, welcomeText, buildEmail(toName, toEmail))
	message.SetHtml(welcomeHTML)
	_, _, err := c.mg.Send(message)
	return err
}

// ResetPw will
func (c *Client) ResetPw(toEmail, token string) error {
	// Set the token within url.values and encode it to build the full reset url
	v := url.Values{}
	v.Set("token", token)
	resetUrl := resetBaseURL + "?" + v.Encode()
	resetText := fmt.Sprintf(resetTextTmpl, resetUrl, token)
	message := mailgun.NewMessage(c.from, resetSubject, resetText, toEmail)
	resetHTML := fmt.Sprintf(resetHTMLTmpl, resetUrl, resetUrl, token)
	message.SetHtml(resetHTML)
	_, _, err := c.mg.Send(message)
	return err
}

func buildEmail(name, email string) string {
	if name == "" {
		return email
	}
	return fmt.Sprintf("%s <%s>", name, email)
}
