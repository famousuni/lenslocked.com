module lenslocked.com

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/csrf v1.7.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/schema v1.2.0 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/keegancsmith/rpc v1.3.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/stamblerre/gocode v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/oauth2 v0.0.0-20210628180205-a41e5a781914 // indirect
	golang.org/x/sys v0.0.0-20210611083646-a4fc73990273 // indirect
	golang.org/x/tools v0.1.3 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/mailgun/mailgun-go.v1 v1.1.1 // indirect
)
