package middleware

import (
	"net/http"
	"strings"

	"lenslocked.com/context"
	"lenslocked.com/models"
)

type User struct {
	models.UserService
}

// Apply takes in an http.Handler (a view object implemnts this) and returns
// ApplyFn with the Handlers HandleFunc/ServeHTTP passed in.
// This is taken in main.go as a handler because the return type http.HandlerFunc
// implements the http.Handler interface (It has the ServeHTTP method.)
func (mw *User) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}

// ApplyFn is used to add the User obj to the context if it exists
func (mw *User) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		// If the user is requesting a static asset or image we will not need
		// to lookup the current user so we skip doing that.
		if strings.HasPrefix(path, "/assets/") ||
			strings.HasPrefix(path, "/images/") {
			next(w, r)
			return
		}
		// If the cookie is not found then call the handler func
		cookie, err := r.Cookie("remember_token")
		if err != nil {
			next(w, r)
			return
		}
		// Lookup the user by the Remember cookie value.
		user, err := mw.UserService.ByRemember(cookie.Value)
		if err != nil {
			next(w, r)
			return
		}
		ctx := r.Context()
		// Add found user to the context
		ctx = context.WithUser(ctx, user)
		// Update the request with the new context
		r = r.WithContext(ctx)
		// Response writer and request are passed to the HandleFunc
		// ex. Now the View or real Function can be rendered/executed.
		next(w, r)
	})
}

// RequireUser assumes that User has already been run
// otherwise it will not work correctly
type RequireUser struct {
	User
}

// Apply assumes that User middleware has already been run
// otherwise it will not work correctly
func (mw *RequireUser) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}

// ApplyFn takes in a HandlerFunc func(w http.ResponseWriter, r *http.Request)
// (in this case ServeHTTP from a view) and returns a dynamic HandlerFunc.
// Since the return is cast as such it satisfies the Handler type.
func (mw *RequireUser) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	// This anonymous function checks for the User obj within the context.
	// If it does not exist it redirects to /login
	// It is cast to http.Handlerfunc
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := context.User(r.Context())
		if user == nil {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		next(w, r)
	})
}
